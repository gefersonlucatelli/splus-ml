
"""
 _      _  _
| |    (_)| |__   ___
| |    | || '_ \ / __|
| |___ | || |_) |\__ \
|_____||_||_.__/ |___/

"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
from sklearn.metrics import make_scorer
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from yellowbrick.contrib.classifier import DecisionViz
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score,cross_val_predict
from sklearn.tree import export_graphviz
from sklearn.metrics import roc_auc_score,roc_curve,precision_recall_fscore_support,fbeta_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import plot_precision_recall_curve
from sklearn.preprocessing import label_binarize
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
import pydot
import seaborn as sns
print("Loading Libs...")


from sklearn import tree
import graphviz

psave = "images/"
psave2 = "."
psave_drop = "/home/lucatelli/Dropbox/Aplicativos/Overleaf/MNRAS-geferson_morphology_splus_paper/images/"
psave_drop2 = "/home/sagauga/Dropbox/Aplicativos/Overleaf/MNRAS-geferson_morphology_splus_paper/images/"
#Fancy config for plots.
try:
#     !pip install jupyterthemes
#     !pip install --upgrade jupyterthemes
#     from jupyterthemes import jtplot
#     jtplot.style(theme='grade3')
#     plt.style.use('seaborn-colorblind')
    plt.style.use('bmh')
#     jtplot.style('monokai')

#     jtplot.style(grid=False, ticks=True, spines=True,gridlines='--')
    from matplotlib import rcParams,rc
    rcParams['font.family'] = 'sans-serif'
    rcParams['font.sans-serif'] = ['Cormorant Infant']
#     rcParams['font.sans-serif'] = ['Garamond']
    plt.rc('xtick', labelsize=22)#,color="magenta")
    plt.rc('ytick', labelsize=22)#,color="magenta")
    #     rc('text', usetex=True)
except:
    pass

CBc = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

def iimshow(img, sigma=3, contours=0, bar=None, aspect='equal', extent=None, vmin=None, vmax=None, use_median=False):
    """
    improved version of pl.imshow,

    shows image with limits sigma above and below mean.

    optionally show contours and colorbar

    From MFMTK.
    """

    def mad(x):
       return np.median( np.abs( x - np.median(x)) )

    # deals with NaN and Infinity
    img[np.where(np.isnan(img))]=0.0
    img[np.where(np.isinf(img))]=0.0

    # wether to use median/mad or mean/stdev.
    # note that mad ~ 1.5 stdev
    if use_median==False:
      if vmin==None:
         vmin = img.mean() - sigma * img.std()
      if vmax==None:
         vmax = img.mean() + sigma * img.std()
    else:
      if vmin==None:
         vmin = np.median(img) - 1.5*sigma * mad(img)
      if vmax==None:
         vmax = np.median(img) + 1.5*sigma * mad(img)


    pl.imshow(img, vmin=vmin, vmax=vmax, origin='lower', aspect=aspect, extent=extent, interpolation=None)

    if bar != None:
        pl.colorbar(pad=0)

    if contours >0:
        pl.contour(img, contours, colors='k', linestyles='solid', aspect=aspect, extent=extent)

def normalise(x):
    return (x- x.min())/(x.max() - x.min())
def contrast_enhance(image):
    """
    Just a simply manipulation for improve the contrast of images.
    """
    _image = (((image)))
    # print log_image
    where_are_NaNs = np.isnan(_image)
    where_are_infs = np.isinf(_image)
    _image[where_are_NaNs] = 100 		 #gambiarra
    _image[where_are_infs] = 100			 #gambiarra
    where_are_100 = np.where(_image==100) #gambiarra
    _image[where_are_100] = _image.min()
    nu_0 = normalise(_image)
    return nu_0 #the enhanced image in log scale.

def get_data(param,File,HEADER=0):
    """
    Get a numerical variable from a table.

    HEADER: if ==1, display the file's header.
    """
    infile = open(File, 'r')
    firstLine = infile.readline()
    header=firstLine.split(',')
    if HEADER==1:
        print(header)
        return header
    else:
        ind=header.index(param)
        return(np.loadtxt(File,usecols=(ind),comments="#", delimiter=",", \
            unpack=False))

def getstr(string,File,HEADER=0):
    """
    Get a string column from a table.
    Also works for floats and ints.
    """
    infile = open(File, 'r')
    firstLine = infile.readline()
    header=firstLine.split(',')
    if HEADER==1:
        print(header)
        return header
    else:
        ind=header.index(string)
        return np.loadtxt(File,dtype='str',usecols=(ind),comments="#", \
            delimiter=",", unpack=False)






def get_from_drive(orig_url):
    """
    Read csv file from drive and returns the data in pd.dataframe format.
    #https://stackoverflow.com/questions/56611698/pandas-how-to-read-csv-file-from-google-drive-public
    """
    # url="https://lucatelli.pro.br/files/data/R_all_data.csv"
    # s=requests.get(url).content
    # c=pd.read_csv(s)

    import requests
    from io import StringIO

    # orig_url='https://drive.google.com/file/d/1Plyi9gij4UdKH6Nd24f43bHLyfzDO5Oz/view?usp=sharing'

    file_id = orig_url.split('/')[-2]
    dwn_url='https://drive.google.com/uc?export=download&id=' + file_id
    url = requests.get(dwn_url).text
    csv_raw = StringIO(url)
    dfs = pd.read_csv(csv_raw)
    print('Load done')
    return(dfs)


# def reduce_df(df,scale=False):
#     """This function is used to reduce a data frame with wanted measuremens.
#     It will divide it between the labels Y and the measurements X. Also,
#     all existing NaN values will be replaced by zero. This works
#     for duplicate collumns."""
#     dfC = df.copy()
#     dfC = dfC.replace([np.inf, -np.inf], np.nan)
#     y = np.array(dfC['class_'])
#     X = dfC.drop("class_",axis=1)
#     X = dfC.fillna(0)
#     #take only numbers.
#     X = X.select_dtypes(include=[np.number])
#     # if scale==True:
#     #     scaler = StandardScaler()
#     #     scaler.fit(X)
#     #     X_ = scaler.transform(X)
#     #     X = pd.DataFrame(X_,columns=list(X.columns))
        
#     return(X,y)

def reduce_df(df,scale=False):
    """This function is used to reduce a data frame with wanted measuremens.
    It will divide it between the labels Y and the measurements X. Also,
    all existing NaN values will be replaced by zero. This works
    for duplicate collumns."""
    dfC = df.copy()
    dfC = dfC.replace([np.inf, -np.inf], np.nan)
    y = np.array(dfC['class_'])
    X = dfC.drop("class_",axis=1)
    X = dfC.fillna(0)
    #take only numbers.
    X = X.set_index("#rootname910")
    X = X.select_dtypes(include=[np.number])
    # if scale==True:
    #     scaler = StandardScaler()
    #     scaler.fit(X)
    #     X_ = scaler.transform(X)
    #     X = pd.DataFrame(X_,columns=list(X.columns))
        
    return(X,y)

def reduce_df_uncl(df,scale=False):
    """This function is used to reduce a data frame with wanted measuremens.
    It will divide it between the labels Y and the measurements X. Also,
    all existing NaN values will be replaced by zero. This works
    for duplicate collumns."""
    dfC = df.copy()
    dfC = dfC.replace([np.inf, -np.inf], np.nan)
    X = dfC.fillna(0)
    #take only numbers.
    X = X.set_index("# rootname910")
    X = X.select_dtypes(include=[np.number])
    # if scale==True:
    #     scaler = StandardScaler()
    #     scaler.fit(X)
    #     X_ = scaler.transform(X)
    #     X = pd.DataFrame(X_,columns=list(X.columns))
        
    return(X)


def reduce_df_multi(df):
    """This function is used to reduce a data frame with wanted measuremens for
    multiple wavelengths.
    It will divide it between the labels Y and the measurements X. Also,
    all existing NaN values will be replaced by the median. This works
    for duplicate collumns."""
    dfC = df.copy()
    y = np.array(dfC['class_'])
    X = dfC.drop("class_",axis=1)
    # X = dfC.fillna(0)
    #take only numbers.
    X = X.select_dtypes(include=[np.number])
    #replace all nans with the median instead of zeros.
    list_to_replace = np.unique(list(X.columns))
    for LL in list_to_replace:
        X[LL] = X[LL].apply(lambda x: x.fillna(x.median()),axis=1)
    return(X,y)


def find_best_model(X,Y,pipe=None,param_grid=None,cv=3):
    """
    X: Data frame with all features.
    Y: feature labels.
    pipe: pipeline of classifier and params.
    param_grid: grid parameters for search GSCV.
    """
    from sklearn.model_selection import GridSearchCV
    from sklearn.pipeline import Pipeline
    clf = GridSearchCV(pipe, param_grid = param_grid, cv = cv, verbose=True, n_jobs=-1)
    best_clf = clf.fit(X, Y)
    best_model = best_clf.best_estimator_
    best_parameters = best_clf.best_params_
    return(best_model,best_parameters)

def find_best_model_rev(X,Y,pipe=None,param_grid=None,cv=3):
    """
    X: Data frame with all features.
    Y: feature labels.
    pipe: pipeline of classifier and params.
    param_grid: grid parameters for search GSCV.
    """
    from sklearn.model_selection import GridSearchCV
    from sklearn.pipeline import Pipeline
    clf = GridSearchCV(pipe, param_grid = param_grid, cv = cv, verbose=True, n_jobs=-1)
    best_clf = clf.fit(X, Y)
    best_model = best_clf.best_estimator_
    best_parameters = best_clf.best_params_
    return(best_model,best_parameters)

def return_best_set(X,y,model_input,param_grid={'min_samples_split': range(2, 403, 10)}):
    scoring = {'AUC': 'roc_auc', 'Accuracy': make_scorer(accuracy_score)}
    gs = GridSearchCV(model_input,
                      param_grid=param_grid,
                      scoring=scoring, refit='AUC', return_train_score=True,verbose=True,n_jobs=-1)
    gs.fit(X, y)
    results = gs.cv_results_
    model = gs.best_estimator_
    return model

# def make_tree_from_best_model(X,Y,save=None,**model_params):
def make_tree_from_best_model(X,Y,save=None,model=DecisionTreeClassifier()):
    # clf = tree.DecisionTreeClassifier(**model_params)
    clf = model
    clf = clf.fit(X, Y)
    tree.plot_tree(clf)

    dot_data = tree.export_graphviz(clf, out_file="Tree_Er_S0")
    graph = graphviz.Source(dot_data)

    dot_data = tree.export_graphviz(clf, out_file=None,
                         feature_names=list(X.columns),
                         class_names=np.unique(Y),
                         filled=True, rounded=True,
                         special_characters=True)
    graph = graphviz.Source(dot_data)
    if save is not None:
        graph.render(psave+save)
    return graph

def plot_decision_boundaries(X, y,model):
    """
    Adopted from:
    https://gist.github.com/anandology/772d44d291a9daa198d4
    http://scikit-learn.org/stable/auto_examples/ensemble/plot_voting_decision_regions.html
    http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html

    Function to plot the decision boundaries of a classification model.
    This uses just the first two columns of the data for fitting
    the model as we need to find the predicted value for every point in
    scatter plot.

    One possible improvement could be to use all columns fot fitting
    and using the first 2 columns and median of all other columns
    for predicting.
    """
    reduced_data = X#[:, :2]
    y = np.asarray(y).astype(np.int)
#     model = model_class(**model_params)
    model.fit(reduced_data, y)
#     fig, ax = plt.subplots()
    fig = plt.figure()
    ax  = fig.add_subplot(111)
#     plt.figure(figsize=(12,8))
    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .01     # point in the mesh [x_min, m_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data.iloc[:, 0].min() - 0.5, reduced_data.iloc[:, 0].max() + 0.5
    y_min, y_max = reduced_data.iloc[:, 1].min() - 0.5, reduced_data.iloc[:, 1].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh using the model.
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

    x_min, x_max = X.iloc[:, 0].min() - 0.5, X.iloc[:, 0].max() + 0.5
    y_min, y_max = X.iloc[:, 1].min() - 0.5, X.iloc[:, 1].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.05),
                         np.arange(y_min, y_max, 0.05))

    Z = model.predict(np.c_[xx.ravel(), yy.ravel()]).reshape(xx.shape)

    ax.contourf(xx, yy, Z, alpha=0.25,cmap='plasma')

    labs = np.unique(y.astype(np.int))
    cl1 = np.where(y.astype(np.int)==labs[0])[0]
    cl2 = np.where(y.astype(np.int)==labs[1])[0]

    ax.scatter(X.iloc[cl1].iloc[:, 0], X.iloc[cl1].iloc[:, 1], c="purple", alpha=0.8,label=labs[0])
    ax.scatter(X.iloc[cl2].iloc[:, 0], X.iloc[cl2].iloc[:, 1], c="yellow", alpha=1.0,label=labs[1])

#     ax.set_xlim(0.0,1.0)
#     ax.set_ylim(0.0,1.0)
    plt.legend()
    plt.grid(color='black',ls="-.")
    # plt.labels
    return plt

def test_acu(X,Y):
    X_train, X_test, y_train, y_test = train_test_split(X, Y, stratify=Y, random_state=42)
    training_accuracy = []
    test_accuracy = []
    # try n_neighbors from 1 to 10
    neighbors_settings = range(1, 11)
    for n_neighbors in neighbors_settings:
        # build the model
        clf = KNeighborsClassifier(n_neighbors=n_neighbors)
        clf.fit(X_train, y_train)
        # record training set accuracy
        training_accuracy.append(clf.score(X_train, y_train))
        # record generalization accuracy
        test_accuracy.append(clf.score(X_test, y_test))
    plt.plot(neighbors_settings, training_accuracy, label="training accuracy")
    plt.plot(neighbors_settings, test_accuracy, label="test accuracy")
    plt.ylabel("Accuracy")
    plt.xlabel("n_neighbors")
    plt.legend()

def best_KNC(df):
    y = np.array(df['class_'])
    X = df.drop("class_",axis=1)
    X = df.fillna(0)
    X = X.select_dtypes(include=[np.number])
    param_grid = {'n_neighbors': np.arange(1,10)}
    grid = GridSearchCV(KNeighborsClassifier(), param_grid, cv=5)
    grid.fit(X, y)
    grid.best_params_
    return grid.best_estimator_,X,y

# def plot_decision_boundaries(X, y, model_class, **model_params):
#     """
#     Adopted from:
#     https://gist.github.com/anandology/772d44d291a9daa198d4
#     http://scikit-learn.org/stable/auto_examples/ensemble/plot_voting_decision_regions.html
#     http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html

#     Function to plot the decision boundaries of a classification model.
#     This uses just the first two columns of the data for fitting
#     the model as we need to find the predicted value for every point in
#     scatter plot.

#     One possible improvement could be to use all columns fot fitting
#     and using the first 2 columns and median of all other columns
#     for predicting.
#     """
#     reduced_data = X#[:, :2]
#     y = np.asarray(y).astype(np.int)
#     model = model_class(**model_params)
#     model.fit(reduced_data, y)
#     plt.figure(figsize=(12,8))
#     # Step size of the mesh. Decrease to increase the quality of the VQ.
#     h = .005     # point in the mesh [x_min, m_max]x[y_min, y_max].

#     # Plot the decision boundary. For that, we will assign a color to each
#     x_min, x_max = reduced_data.iloc[:, 0].min() - 0.5, reduced_data.iloc[:, 0].max() + 0.5
#     y_min, y_max = reduced_data.iloc[:, 1].min() - 0.5, reduced_data.iloc[:, 1].max() + 0.5
#     xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

#     # Obtain labels for each point in mesh using the model.
#     Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

#     x_min, x_max = X.iloc[:, 0].min() - 0.5, X.iloc[:, 0].max() + 0.5
#     y_min, y_max = X.iloc[:, 1].min() - 0.5, X.iloc[:, 1].max() + 0.5
#     xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
#                          np.arange(y_min, y_max, h))

#     Z = model.predict(np.c_[xx.ravel(), yy.ravel()]).reshape(xx.shape)

#     plt.contourf(xx, yy, Z, alpha=0.2)
#     plt.scatter(X.iloc[:, 0], X.iloc[:, 1], c=y, alpha=0.8)
#     plt.xlim(0.0,1.0)
#     plt.ylim(0.0,1.0)
#     plt.grid()
#     # plt.labels
#     return plt

from sklearn.model_selection import learning_curve, KFold
def plot_learning_curve(est, X, y,cvf=3,title="Learning Curve",save=None,plus_info=""):
    training_set_size, train_scores, test_scores = learning_curve(
        est, X, y, train_sizes=np.linspace(.05, 1, 30), cv=KFold(cvf, shuffle=True, random_state=42), n_jobs=-1)
    estimator_name = est.__class__.__name__
    line = plt.plot(training_set_size/len(X), train_scores.mean(axis=1), ls='--', lw=2,
                    label="training " + estimator_name+" $-$ "+plus_info+"")
    plt.plot(training_set_size/len(X), test_scores.mean(axis=1), ls='-',lw=2,
             label="test " + estimator_name, c=line[0].get_color())
    plt.xlabel('Training set size')
    plt.ylabel('Score ($R^2$)')
    plt.title(r""+title+"")
    plt.legend(prop={"size":14})
    if save is not None:
        plt.savefig(save,dpi = 300,bbox_inches='tight')
    plt.ylim(0, 1.1)
    return(plt)

# def plot_region(X,Y,model_class,**model_params):
def plot_region(X,Y,model):
#     plt.figure(figsize=(13, 8))
    fig = plt.figure(figsize=(10,7))
    ax = fig.add_subplot(111)
    h = .005
    x = np.asarray(X)
    y = Y.astype(np.int)
    # model = model_class(**model_params)
    model.fit(x,y)
    # svc = svm.SVC(kernel='linear',C=1.0).fit(x,y)
    # svc = svm.SVC(kernel='poly',C=1.0,degree=3).fit(x,y)
#     svc = svm.SVC(kernel='rbf', gamma=3, C=1.0).fit(x,y)
    x_min,x_max = x[:,0].min() - .5, x[:,0].max() + .5
    y_min,y_max = x[:,1].min() - .5, x[:,1].max() + .5
    X, Y = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min,y_max,h))
    Z = model.predict(np.c_[X.ravel(),Y.ravel()])
    Z = Z.reshape(X.shape)
    plt.contourf(X,Y,Z,alpha=0.4,cmap='plasma')
    plt.contour(X,Y,Z,colors='k')
#     plt.scatter(x[:,0],x[:,1],c=y)

    labs = np.unique(y.astype(np.int))
    cl1 = np.where(y.astype(np.int)==labs[0])[0]
    ax.scatter(x[:, 0][cl1], x[:, 1][cl1], c="purple", alpha=0.8,label=labs[0])
    cl2 = np.where(y.astype(np.int)==labs[1])[0]
    ax.scatter(x[:, 0][cl2], x[:, 1][cl2], c="yellow", alpha=0.8,label=labs[1])
    if labs.shape[0]>2:
        cl3 = np.where(y.astype(np.int)==labs[2])[0]
        ax.scatter(x[:, 0][cl3], x[:, 1][cl3], c="green", alpha=0.8,label=labs[2])
    plt.legend(prop={"size":18})
    return plt

# def validate(df,input_model,param_grid,par_label,save=None):
def validate(X,y,input_model,param_grid,par_label,save=None):
    # y = np.array(df['class_'])
    # X = df.drop("class_",axis=1)
    # X = df.fillna(0)
    # X = X.select_dtypes(include=[np.number])
#     X = X[VARS2]

#     RandomForestClassifier
#     DecisionTreeClassifier(random_state=42)
    scoring = {'AUC': 'roc_auc', 'Accuracy': make_scorer(accuracy_score)}
    gs = GridSearchCV(input_model,cv=5,
                      param_grid=param_grid,
                      scoring=scoring, refit='AUC', return_train_score=True,n_jobs=-1,verbose=True)
    best_clf = gs.fit(X, y)
    best_model = best_clf.best_estimator_
    print(best_model)



    def make_plot_validation():
        plt.figure(figsize=(8, 8))
        plt.title("GridSearchCV evaluating using multiple scorers simultaneously",
                  fontsize=16)

        plt.xlabel(par_label)
        plt.ylabel("Score")

        ax = plt.gca()
#         ax.set_xlim(0, 1000)
        ax.set_ylim(0.73, 1)

        gs.fit(X, y)
        results = gs.cv_results_

        # Get the regular numpy array from the MaskedArray
        X_axis = np.array(results['param_'+par_label+''].data, dtype=float)

        for scorer, color in zip(sorted(scoring), ['g', 'k']):
            for sample, style in (('train', '--'), ('test', '-')):
                sample_score_mean = results['mean_%s_%s' % (sample, scorer)]
                sample_score_std = results['std_%s_%s' % (sample, scorer)]
                ax.fill_between(X_axis, sample_score_mean - sample_score_std,
                                sample_score_mean + sample_score_std,
                                alpha=0.1 if sample == 'test' else 0, color=color)
                ax.plot(X_axis, sample_score_mean, style, color=color,
                        alpha=1 if sample == 'test' else 0.7,
                        label="%s (%s)" % (scorer, sample))

            best_index = np.nonzero(results['rank_test_%s' % scorer] == 1)[0][0]
            best_score = results['mean_test_%s' % scorer][best_index]

            # Plot a dotted vertical line at the best score for that scorer marked by x
            ax.plot([X_axis[best_index], ] * 2, [0, best_score],
                    linestyle='-.', color=color, marker='x', markeredgewidth=3, ms=8)

            # Annotate the best score for that scorer
            ax.annotate("%0.2f" % best_score,
                        (X_axis[best_index], best_score + 0.005))

        plt.legend(loc="best")
        plt.grid(False)
        plt.grid(color='grey',alpha=0.2)
        if save is not None:
            plt.savefig(psave+save,dpi = 600,bbox_inches='tight')
        plt.show()
    make_plot_validation()
    return(best_model,gs)

from sklearn import svm
def find_best_model_SVM(X,Y,model,param_grid=None):
    """
    X: Data frame with all features.
    Y: feature labels.
    pipe: pipeline of classifier and params.
    param_grid: grid parameters for search GSCV.
    """
#     X = np.asarray(X)
    from sklearn.model_selection import GridSearchCV
    clf = GridSearchCV(model,param_grid = param_grid, cv = 5, verbose=True, n_jobs=-1)
    best_clf = clf.fit(X, Y)
    best_model = best_clf.best_estimator_
    return(best_model)


# def RF_classifier(df,ne=500,mss=2,max_feat=3,md=4,re=0,save_name='result'):
def RF_classifier(X,y,save_name=None,**mod_pars):
    #https://www.datacamp.com/community/tutorials/random-forests-classifier-python
    from sklearn.model_selection import train_test_split
    from sklearn.ensemble import RandomForestClassifier
    from sklearn import metrics
    # df = df.replace(np.nan,0)
#     df.describe()
#     y = df['class_']
#     df= df.select_dtypes(include=[np.number])
    # X = df

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

    clf=RandomForestClassifier(**mod_pars)
#     clf_region=RandomForestClassifier(n_estimators=ne)

#     clf_region.fit(np.asarray(X),np.asarray(y).astype(np.int))

    clf.fit(X_train,y_train)

    y_pred=clf.predict(X_test)
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))

    index = list(X.columns)
    feature_imp = pd.Series(clf.feature_importances_,index=index).sort_values(ascending=False)
    feature_imp

    def make_plot():
#         import seaborn as sns
#         %matplotlib inline
        # Creating a bar plot
        plt.figure(figsize=(6,8))
        sns.barplot(x=feature_imp, y=r''+feature_imp.index+'')
        # Add labels to your graph
        plt.xlabel('Morphometric Index Importance',size=16)
        plt.ylabel('Morphometric Index',size=16)

        plt.grid(which='major', axis='both', linestyle='--',color="gray",alpha=0.5)
        if save_name is not None:
            plt.title("Visualizing Important Morphometric Indices: "+save_name,size=16)
            plt.savefig(save_name+"_RF_mf_features.pdf",dpi = 600,bbox_inches='tight')
        plt.show()
    make_plot()
#     fig = plot_decision_regions(X=np.asarray(X), y=np.asarray(y).astype(np.int), clf=clf_region,zoom_factor=5,legend=2)


# def RF_tree(df,ne=500,mss=2,max_feat=3,md=4,re=0,msl=1,save_name="result"):
def RF_tree(X,y,save_name="result",**mod_pars):
    #https://www.datacamp.com/community/tutorials/random-forests-classifier-python
    from sklearn.model_selection import train_test_split
    from sklearn.ensemble import RandomForestClassifier
    from IPython.display import Image
    from sklearn import metrics
    # df = df.replace(np.nan,0)
#     df.describe()
#     y = df['class_']
#     df= df.select_dtypes(include=[np.number])
    # X = df

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.01)

#     sc = StandardScaler()
#     X_train = sc.fit_transform(X_train)
#     X_test = sc.transform(X_test)

    clf = RandomForestRegressor(**mod_pars)
    clf.fit(X_train,y_train)

    scores = cross_val_score(clf, X_train, y_train, cv=5)
    print("Scores",scores)

    y_pred=clf.predict(X_test)
    predictions = clf.predict(X_test)
    index = list(X.columns)
#     print(metrics.accuracy_score(y_test, y_pred))

    def make_tree():
        tree = clf.estimators_[0]
        export_graphviz(tree, out_file = save_name+'.dot', feature_names = index, precision = 1,filled=True,rounded=True,special_characters=True)
        (graph, ) = pydot.graph_from_dot_file(save_name+'.dot')
        graph.write_pdf(save_name+"_tree.pdf")
    make_tree()

# def plot_region(X,Y,model_class,**model_params):
#     plt.figure(figsize=(13, 8))
#     h = .005
#     x = np.asarray(X)
#     y = Y.astype(np.int)
#     model = model_class(**model_params)
#     model.fit(x,y)
#     # svc = svm.SVC(kernel='linear',C=1.0).fit(x,y)
#     # svc = svm.SVC(kernel='poly',C=1.0,degree=3).fit(x,y)
# #     svc = svm.SVC(kernel='rbf', gamma=3, C=1.0).fit(x,y)
#     x_min,x_max = x[:,0].min() - .5, x[:,0].max() + .5
#     y_min,y_max = x[:,1].min() - .5, x[:,1].max() + .5
#     h = .02
#     X, Y = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min,y_max,h))

# #     xx = np.arange(0.0, 1.0, h)
# #     yy = np.arange(0.0, 1.0, h)
# #     X, Y = np.meshgrid(x, y)

#     Z = model.predict(np.c_[X.ravel(),Y.ravel()])
#     Z = Z.reshape(X.shape)
#     cmap = mpl.cm.cool
#     norm = mpl.colors.Normalize(vmin=5, vmax=10)
#     mycmap2 = plt.get_cmap('gnuplot2')
#     plt.contour(X,Y,Z)
#     plt.contourf(X,Y,Z,alpha=0.4,cmap=mycmap2)
#     plt.scatter(x[:,0],x[:,1],c=y)
#     return plt


def kfold_make_roc_curve(X,y,classifier,title='',info_plus = '',save_name=None):
    n_samples, n_features = X.shape
    from sklearn.metrics import plot_roc_curve
    from sklearn.metrics import auc
    XX=np.asarray(X)
    # #############################################################################
    # Classification and ROC analysis
    # Run classifier with cross-validation and plot ROC curves
    cv = StratifiedKFold(n_splits=5)
    if classifier == None:
        classifier = svm.SVC(kernel='linear', probability=True,
                             random_state=random_state)
    else:
        classifier = classifier

    tprs = []
    aucs = []
    mean_fpr = np.linspace(0, 1, 100)

    fig, ax = plt.subplots()
    for i, (train, test) in enumerate(cv.split(XX, y)):
        classifier.fit(XX[train], y[train])
        viz = plot_roc_curve(classifier, XX[test], y[test],
                             name='fold {}'.format(i),
                             alpha=0.3, lw=2, ax=ax)
        interp_tpr = np.interp(mean_fpr, viz.fpr, viz.tpr)
        interp_tpr[0] = 0.0
        tprs.append(interp_tpr)
        aucs.append(viz.roc_auc)

    ax.plot([0, 1], [0, 1], linestyle='--', lw=3, color='r', alpha=.8)

    mean_tpr = np.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)
    ax.plot(mean_fpr, mean_tpr, color='b',
            label=r'Mean (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
            lw=3, alpha=.8)

    std_tpr = np.std(tprs, axis=0)
    tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
    ax.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.3,
                    label=r'$\pm$ 1 std. dev.')

    ax.set(xlim=[-0.05, 1.05], ylim=[-0.05, 1.05],
           title=r""+title+" $-$ "+info_plus)
    ax.legend(loc="lower right")
    plt.grid()
    if save_name is not None:
        plt.savefig(save_name,dpi = 600,bbox_inches='tight')
    return(ax)



# def multi_complex_ROC(X,y,X_train,y_train,X_test,y_test,model_best):
#     from sklearn.metrics import auc
#     "y with binarized labels"
#     model_best.fit(X_train, y_train)
#     y_score = model_best.predict_proba(X_test)
#     y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
# #     y_score_n = np.asarray([y_score[:,0],y_score[:,1]]).T
#     n_classes = y.shape[1]
#     n_samples, n_features = X.shape
#     fpr = dict()
#     tpr = dict()
#     roc_auc = dict()
#     for i in range(n_classes):
#         fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score[i][:,1])
# #         fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score_n[:,i])
#         roc_auc[i] = auc(fpr[i], tpr[i])
#     fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score_n.ravel())
#     roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
#
#     plt.figure()
#     lw = 2
#     plt.plot(fpr[2], tpr[2], color='darkorange',
#     lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[2])
#     plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.title('Receiver operating characteristic example')
#     plt.legend(loc="lower right")
#     plt.show()
#
#     # First aggregate all false positive rates
#     all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
#     # Then interpolate all ROC curves at this points
#     mean_tpr = np.zeros_like(all_fpr)
#     for i in range(n_classes):
#         mean_tpr += interp(all_fpr, fpr[i], tpr[i])
#     # Finally average it and compute AUC
#     mean_tpr /= n_classes
#     fpr["macro"] = all_fpr
#     tpr["macro"] = mean_tpr
#     roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
#     # Plot all ROC curves
#     plt.figure()
#     plt.plot(fpr["micro"], tpr["micro"],
#         label='micro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["micro"]),
#         color='deeppink', linestyle=':', linewidth=4)
#     plt.plot(fpr["macro"], tpr["macro"],
#         label='macro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["macro"]),
#         color='navy', linestyle=':', linewidth=4)
#     colors = cycle(['aqua', 'darkorange', 'cornflowerblue'])
#     for i, color in zip(range(n_classes), colors):
#         plt.plot(fpr[i], tpr[i], color=color, lw=lw,
#             label='ROC curve of class {0} (area = {1:0.2f})'
#             ''.format(i, roc_auc[i]))
#     plt.plot([0, 1], [0, 1], 'k--', lw=lw)
#     plt.xlim([0.0, 1.0])
#     plt.ylim([0.0, 1.05])
#     plt.xlabel('False Positive Rate')
#     plt.ylabel('True Positive Rate')
#     plt.title('Some extension of Receiver operating characteristic to multi-class')
#     plt.legend(loc="lower right")
#     plt.grid()
#     plt.show()


    # pipe = Pipeline([('classifier' , DecisionTreeClassifier())])
# param_grid = [{'classifier' : [DecisionTreeClassifier()],
# #     'classifier__n_estimators' : [750],#list(range(500,1000,250)),
#     'classifier__criterion' : ["entropy","gini"],
#     'classifier__max_depth': list(range(1,4,1)),
#     'classifier__min_samples_split': list(range(2,100,10)),
#     'classifier__min_samples_leaf': list(range(2,10,2)),
#     'classifier__max_features' : list(range(2,len(VARS[:-1]),1)),
#               }]
# model_best,model_best_pars = find_best_model(X[['C1','H']],Y,pipe,param_grid)
# model_best = model_best['classifier']
# print(model_best)
# # print(model_best.score(X,Y))
# # plt.figure()
# # plot_feature_importances(X,Y,VARS,tree_ml)
# # plt.title("Not best model")
# # plt.figure()
# # plot_feature_importances(X,Y,VARS,model_best)
# # plt.title("best model")
# # print(model_best)

def plot_decision_boundaries_m(X, y,model):
    """
    Adopted from:
    https://gist.github.com/anandology/772d44d291a9daa198d4
    http://scikit-learn.org/stable/auto_examples/ensemble/plot_voting_decision_regions.html
    http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html

    Function to plot the decision boundaries of a classification model.
    This uses just the first two columns of the data for fitting
    the model as we need to find the predicted value for every point in
    scatter plot.

    One possible improvement could be to use all columns fot fitting
    and using the first 2 columns and median of all other columns
    for predicting.
    """
    reduced_data = X#[:, :2]
    y = np.asarray(y).astype(np.int)
#     model = model_class(**model_params)
    model.fit(reduced_data, y)
#     fig, ax = plt.subplots()
    fig = plt.figure()
    ax  = fig.add_subplot(111)
#     plt.figure(figsize=(12,8))
    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .01     # point in the mesh [x_min, m_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data.iloc[:, 0].min() - 0.5, reduced_data.iloc[:, 0].max() + 0.5
    y_min, y_max = reduced_data.iloc[:, 1].min() - 0.5, reduced_data.iloc[:, 1].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh using the model.
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

    x_min, x_max = X.iloc[:, 0].min() - 0.5, X.iloc[:, 0].max() + 0.5
    y_min, y_max = X.iloc[:, 1].min() - 0.5, X.iloc[:, 1].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.05),
                         np.arange(y_min, y_max, 0.05))

    Z = model.predict(np.c_[xx.ravel(), yy.ravel()]).reshape(xx.shape)

    ax.contourf(xx, yy, Z, alpha=0.25,cmap='plasma')

    labs = np.unique(y.astype(np.int))
    print(labs)
    cl1 = np.where(y.astype(np.int)==labs[0])[0]
    cl2 = np.where(y.astype(np.int)==labs[1])[0]
    cl3 = np.where(y.astype(np.int)==labs[2])[0]


    ax.scatter(X.iloc[cl1].iloc[:, 0], X.iloc[cl1].iloc[:, 1], c="purple", alpha=0.8,label=labs[0])
    ax.scatter(X.iloc[cl2].iloc[:, 0], X.iloc[cl2].iloc[:, 1], c="red", alpha=1.0,label=labs[1])
    ax.scatter(X.iloc[cl3].iloc[:, 0], X.iloc[cl3].iloc[:, 1], c="yellow", alpha=1.0,label=labs[2])

#     ax.set_xlim(0.0,1.0)
#     ax.set_ylim(0.0,1.0)
    plt.legend()
    plt.grid(color='black',ls="-.")
    # plt.labels
    return plt


def make_learn_tree(X,Y,model,VARS,class_names,out_file):
    X_train, X_test, y_train, y_test = train_test_split(\
        X, Y, stratify=Y.astype(np.int), random_state=42)
    tree_ml = model
    tree_ml.fit(X_train, y_train)
    print("Accuracy on training set: {:.3f}".format(tree_ml.score(X_train, y_train)))
    print("Accuracy on test set: {:.3f}".format(tree_ml.score(X_test, y_test)))
    # plt.figure()
    eg = export_graphviz(tree_ml, out_file=out_file, class_names=class_names,
    feature_names=VARS[:-1], impurity=False, filled=True)
    with open(out_file) as f:
        dot_graph = f.read()
    graphviz.Source(dot_graph)
    return(graphviz.Source(dot_graph))
    # plt.savefig("outputs/decision_trees/tree_Rband_Er_S0.svg",dpi=300)
#     return(tree)


def make_learn_all(X,Y,model):
    # tree = DecisionTreeClassifier(**model_parms)
    tree=model
    tree.fit(X, Y)
    print("Accuracy on training set: {:.3f}".format(tree.score(X, Y)))
#     print("Accuracy on test set: {:.3f}".format(tree.score(X_test, y_test)))
    return(tree)

def make_learn_train(X,Y,model,VARS,class_names,out_file):
    tree_ml = model
    tree_ml.fit(X, Y)
    print("Accuracy on training set: {:.3f}".format(tree_ml.score(X, Y)))
#     print("Accuracy on test set: {:.3f}".format(tree_ml.score(X_test, y_test)))
    # plt.figure()
    eg = export_graphviz(tree_ml, out_file=out_file, class_names=class_names,
    feature_names=VARS[:-1], impurity=False, filled=True)
    with open(out_file) as f:
        dot_graph = f.read()
    graphviz.Source(dot_graph)
    return(graphviz.Source(dot_graph))
    # plt.savefig("outputs/decision_trees/tree_Rband_Er_S0.svg",dpi=300)
#     return(tree)

def make_learn_train_test(X_train,y_train,X_test,y_test,model,VARS,class_names,out_file):
#     X_train, X_test, y_train, y_test = train_test_split(\
#         X, Y, stratify=Y.astype(np.int), random_state=42)
    tree_ml = model
    tree_ml.fit(X_train, y_train)
    print("Accuracy on training set: {:.3f}".format(tree_ml.score(X_train, y_train)))
    print("Accuracy on test set: {:.3f}".format(tree_ml.score(X_test, y_test)))
    # plt.figure()
    eg = export_graphviz(tree_ml, out_file=out_file, class_names=class_names,
    feature_names=VARS[:-1], impurity=False, filled=True)
    with open(out_file) as f:
        dot_graph = f.read()
    graphviz.Source(dot_graph)
    return(graphviz.Source(dot_graph))
    # plt.savefig("outputs/decision_trees/tree_Rband_Er_S0.svg",dpi=300)
#     return(tree)


def make_learn(X,Y,model):
    X_train, X_test, y_train, y_test = train_test_split(\
        X, Y, stratify=Y.astype(np.int), random_state=42)
#     tree = DecisionTreeClassifier(**model_parms)
    tree = model
    tree.fit(X_train, y_train)
    print("Accuracy on training set: {:.3f}".format(tree.score(X_train, y_train)))
    print("Accuracy on test set: {:.3f}".format(tree.score(X_test, y_test)))
    return(tree)

def make_learn_train_test(X_train,y_train,X_test,y_test,model,VARS,class_names,out_file):
#     X_train, X_test, y_train, y_test = train_test_split(\
#         X, Y, stratify=Y.astype(np.int), random_state=42)
    tree_ml = model
    tree_ml.fit(X_train, y_train)
    print("Accuracy on training set: {:.3f}".format(tree_ml.score(X_train, y_train)))
    print("Accuracy on test set: {:.3f}".format(tree_ml.score(X_test, y_test)))
    # plt.figure()
    eg = export_graphviz(tree_ml, out_file=out_file, class_names=class_names,
    feature_names=VARS[:-1], impurity=False, filled=True)
    with open(out_file) as f:
        dot_graph = f.read()
    s=graphviz.Source(dot_graph)
    return(s)

def make_rf_tree(X,Y,X_train,y_train,X_test,y_test,model,VARS,class_names,out_file):
    clf = model
    clf.fit(X_train,y_train)
    scores = cross_val_score(clf, X_train, y_train, cv=5)
    print("Scores",scores)

    y_pred=clf.predict(X_test)
    predictions = clf.predict(X_test)
    index = list(X.columns)
    tree = clf.estimators_[1]
    # tree=model
    tree.random_state = 42
    tree.fit(X_test,y_test)
#     eg=export_graphviz(tree, out_file = out_file,
#                        feature_names = VARS[:-1], precision = 1,
#                        filled=True,rounded=True,
#                        special_characters=True,
#                        class_names=class_names)
    eg = export_graphviz(tree, out_file=out_file, class_names=class_names,
    feature_names=VARS[:-1], impurity=True, filled=True)

#     (graph, ) = pydot.graph_from_dot_file(save_name+'.dot')
    with open(out_file) as f:
        dot_graph = f.read()
    s=graphviz.Source(dot_graph)
    return(s)

#     print(metrics.accuracy_score(y_test, y_pred))

#     def make_tree():
#         tree = clf.estimators_[0]
#         tree.random_state = 42
#         export_graphviz(tree, out_file = save_name+'.dot',
#                         feature_names = index, precision = 1,
#                         filled=True,rounded=True,
#                         special_characters=True,
#                         class_names=class_names)
#         (graph, ) = pydot.graph_from_dot_file(save_name+'.dot')
#         graph.write_pdf(save_name+"_tree.pdf")
# #     make_tree()


#     graph.write_pdf(save_name+"_tree.pdf")



    # plt.savefig("outputs/decision_trees/tree_Rband_Er_S0.svg",dpi=300)
#     return(tree)


def multi_complex_ROC(X,y,X_train,y_train,X_test,y_test,model_best,save_name1=None,save_name2=None,title='title'):
    from sklearn.metrics import auc
    from itertools import cycle
    "y with binarized labels"
    model_best.fit(X_train, y_train)
    try:
        y_score = model_best.predict_proba(X_test)
        y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
        # y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        y_score = model_best.predict(X_test)
        y_score_n = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
#     y_score_n = np.asarray([y_score[:,0],y_score[:,1]]).T
    n_classes = y.shape[1]
    n_samples, n_features = X.shape
    fpr = dict()
    tpr = dict()
    proba = dict()
    roc_auc = dict()
    for i in range(n_classes):
        try:
            fpr[i], tpr[i], proba[i] = roc_curve(y_test[:, i],y_score[i][:,1])
        except:
            fpr[i], tpr[i], proba[i] = roc_curve(y_test[:, i],y_score[:,i])
#         fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score_n[:,i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], proba["micro"] = roc_curve(y_test.ravel(), y_score_n.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    plt.figure()
    lw = 2
    plt.plot(fpr[2], tpr[2], color='darkorange',
    lw=lw, label='ROC (area = %0.2f)' % roc_auc[2])
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r""+title+"")
    plt.legend(loc="lower right")
    plt.grid()
    if save_name1 is not None:
        plt.savefig(save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()

    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])
    # Finally average it and compute AUC
    mean_tpr /= n_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])


    # Plot all ROC curves
    plt.figure()
#     plt.plot(fpr["micro"], tpr["micro"],
#         label='micro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["micro"]),
#         color='deeppink', linestyle=':', linewidth=4)
#     plt.plot(fpr["macro"], tpr["macro"],
#         label='macro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["macro"]),
#         color='navy', linestyle=':', linewidth=4)
    # colors = cycle(['aqua', 'darkorange', 'cornflowerblue','darkmagenta'])
    colors = ['aqua', 'darkorange', 'cornflowerblue','darkmagenta']
    for i, color in zip(range(n_classes), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
            label='class {0} (area = {1:0.2f})'
            ''.format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r''+title+'')
    plt.legend(loc="lower right")
    plt.grid()
    if save_name2 is not None:
        plt.savefig(save_name2,\
                    dpi=300,bbox_inches='tight')
#     plt.savefig(psave_drop2 + "decision_trees/Kfold_DTree_ROC_E_S_S0_2.pdf",\
#                 dpi=300,bbox_inches='tight')

    return fpr,tpr,proba
#     plt.show()

def multi_complex_ROC_train_test(X,y,model_best,save_name1=None,save_name2=None,title='title'):
    from sklearn.metrics import auc
    from itertools import cycle
    "y with binarized labels"
#     model_best.fit(X_train, y_train)
    try:
        y_score = model_best.predict_proba(X)
#         y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
        y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        y_score = model_best.predict(X)
        y_score_n = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
#     y_score_n = np.asarray([y_score[:,0],y_score[:,1]]).T
    n_classes = y.shape[1]
    n_samples, n_features = X.shape
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        try:
            fpr[i], tpr[i], _ = roc_curve(y[:, i],y_score[i][:,1])
        except:
            fpr[i], tpr[i], _ = roc_curve(y[:, i],y_score[:,i])
#         fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score_n[:,i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y.ravel(), y_score_n.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    plt.figure()
    lw = 2
    plt.plot(fpr[2], tpr[2], color='darkorange',
    lw=lw, label='ROC (area = %0.2f)' % roc_auc[2])
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r""+title+"")
    plt.legend(loc="lower right")
    plt.grid()
    if save_name1 is not None:
        plt.savefig(psave_drop2 + save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()

    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])
    # Finally average it and compute AUC
    mean_tpr /= n_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])


    # Plot all ROC curves
    plt.figure()
#     plt.plot(fpr["micro"], tpr["micro"],
#         label='micro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["micro"]),
#         color='deeppink', linestyle=':', linewidth=4)
#     plt.plot(fpr["macro"], tpr["macro"],
#         label='macro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["macro"]),
#         color='navy', linestyle=':', linewidth=4)
    # colors = cycle(['aqua', 'darkorange', 'cornflowerblue','darkmagenta'])
    # colors = ['navy', 'turquoise', 'darkorange', 'cornflowerblue', 'teal']
    colors  = ['aqua', 'darkorange', 'cornflowerblue','darkmagenta']
    for i, color in zip(range(n_classes), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
            label='class {0} (area = {1:0.2f})'
            ''.format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r''+title+'')
    plt.legend(loc="lower right")
    plt.grid()
    if save_name2 is not None:
        plt.savefig(psave_drop2 + save_name2,\
                    dpi=300,bbox_inches='tight')


def precision_recall_isos_multi_rev(X,Y,model_best,save_name1=None,save_name2=None,title=''):
    from itertools import cycle

    # We use OneVsRestClassifier for multi-label prediction

    # model_best.fit(X, Y)
    try:
        y_score = model_best.predict_proba(X)
        y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
        # y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        y_score = model_best.predict_proba(X)
        y_score = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
    n_classes = Y.shape[1]
    n_samples, n_features = X.shape
    #########################################################
    from sklearn.metrics import precision_recall_curve
    from sklearn.metrics import average_precision_score

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    for i in range(n_classes):
        precision[i], recall[i], _ = precision_recall_curve(Y[:, i],
                                                            y_score[:, i])
        average_precision[i] = average_precision_score(Y[:, i], y_score[:, i])

    # A "micro-average": quantifying score on all classes jointly
    precision["macro"], recall["macro"], _ = precision_recall_curve(Y.ravel(),
        y_score.ravel())
    average_precision["macro"] = average_precision_score(Y, y_score,
                                                         average="micro")
    print('Average precision score, micro-averaged over all classes: {0:0.2f}'
          .format(average_precision["macro"]))
    #########################################################

    plt.figure()
    plt.step(recall['macro'], precision['macro'], where='post')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    # plt.title(
        # 'Average precision score, micro-averaged over all classes: AP={0:0.2f}'
        # .format(average_precision["macro"]))
    plt.grid()
    plt.title(r""+title+"")
    if save_name1 is not None:
        plt.savefig(save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()
    # setup plot details
    # colors = ['navy', 'turquoise', 'darkorange', 'cornflowerblue', 'teal']
    colors = ['aqua', 'darkorange', 'cornflowerblue','darkmagenta']
    # colors = cycle(['aqua', 'darkorange', 'cornflowerblue','darkmagenta'])

    plt.figure(figsize=(7, 8))
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    labels = []

    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)
    labels.append('iso-f1 curves')
#     l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
#     lines.append(l)
#     labels.append('micro-average Precision-recall (area = {0:0.2f})'
#                   ''.format(average_precision["micro"]))

    for i, color in zip(range(n_classes), colors):
        l, = plt.plot(recall[i], precision[i], color=color, lw=2)
        lines.append(l)
        labels.append('class {0} (area = {1:0.2f})'
                      ''.format(i, average_precision[i]))

    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.25)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
#     plt.title('Extension of Precision-Recall curve to multi-class')
    plt.title(r""+title+"")
    plt.legend(lines, labels, loc=(0.13, -.43), prop=dict(size=12))
    plt.grid()
    if save_name2 is not None:
        plt.savefig(save_name2,\
                    dpi=300,bbox_inches='tight')
    return plt


def precision_recall_isos_multi(X,Y,X_train,y_train,X_test,y_test,model_best,save_name1=None,save_name2=None,title=''):
    from itertools import cycle

    # We use OneVsRestClassifier for multi-label prediction

    model_best.fit(X_train, y_train)
    try:
        y_score = model_best.predict_proba(X_test)
        y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
        # y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        y_score = model_best.predict(X_test)
        y_score = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
    n_classes = Y.shape[1]
    n_samples, n_features = X.shape
    #########################################################
    from sklearn.metrics import precision_recall_curve
    from sklearn.metrics import average_precision_score

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    for i in range(n_classes):
        precision[i], recall[i], _ = precision_recall_curve(y_test[:, i],
                                                            y_score[:, i])
        average_precision[i] = average_precision_score(y_test[:, i], y_score[:, i])

    # A "micro-average": quantifying score on all classes jointly
    precision["macro"], recall["macro"], _ = precision_recall_curve(y_test.ravel(),
        y_score.ravel())
    average_precision["macro"] = average_precision_score(y_test, y_score,
                                                         average="micro")
    print('Average precision score, micro-averaged over all classes: {0:0.2f}'
          .format(average_precision["macro"]))
    #########################################################

    plt.figure()
    plt.step(recall['macro'], precision['macro'], where='post')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    # plt.title(
        # 'Average precision score, micro-averaged over all classes: AP={0:0.2f}'
        # .format(average_precision["macro"]))
    plt.grid()
    plt.title(r""+title+"")
    if save_name1 is not None:
        plt.savefig(save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()
    # setup plot details
    # colors = ['navy', 'turquoise', 'darkorange', 'cornflowerblue', 'teal']
    colors = ['aqua', 'darkorange', 'cornflowerblue','darkmagenta']
    # colors = cycle(['aqua', 'darkorange', 'cornflowerblue','darkmagenta'])

    plt.figure(figsize=(7, 8))
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    labels = []

    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)
    labels.append('iso-f1 curves')
#     l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
#     lines.append(l)
#     labels.append('micro-average Precision-recall (area = {0:0.2f})'
#                   ''.format(average_precision["micro"]))

    for i, color in zip(range(n_classes), colors):
        l, = plt.plot(recall[i], precision[i], color=color, lw=2)
        lines.append(l)
        labels.append('class {0} (area = {1:0.2f})'
                      ''.format(i, average_precision[i]))

    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.25)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
#     plt.title('Extension of Precision-Recall curve to multi-class')
    plt.title(r""+title+"")
    plt.legend(lines, labels, loc=(0.13, -.43), prop=dict(size=12))
    plt.grid()
    if save_name2 is not None:
        plt.savefig(save_name2,\
                    dpi=300,bbox_inches='tight')
    return plt

#     plt.show()


def precision_recall_isos_two(X,Y,X_train,y_train,X_test,y_test,model_best,save_name1=None,save_name2=None,title=''):
    from itertools import cycle

    # We use OneVsRestClassifier for multi-label prediction

    model_best.fit(X_train, y_train)
    try:
        y_score = model_best.predict_proba(X_test)
        y_score_p = model_best.predict(X_test)
        y_score = np.asarray([y_score[0][:,1],y_score[1][:,1]]).T
#         y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
#         y_score = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        try:
            y_score = model_best.predict(X_test)
            y_score_p = model_best.predict(X_test)
            y_score = np.asarray([y_score[:,0],y_score[:,1]]).T
    #         y_score = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
        except:
            y_score = model_best.predict_proba(X_test)
            y_score_p = model_best.predict(X_test)
            y_score = np.asarray([y_score[:,0],y_score[:,1]]).T
    n_classes = Y.shape[1]
    n_samples, n_features = X.shape
    #########################################################
    from sklearn.metrics import precision_recall_curve
    from sklearn.metrics import average_precision_score

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    for i in range(n_classes):
        precision[i], recall[i], _ = precision_recall_curve(y_test[:, i],
                                                            y_score[:, i])
        average_precision[i] = average_precision_score(y_test[:, i], y_score[:, i])

    # A "micro-average": quantifying score on all classes jointly
    try:
        precision["micro"], recall["micro"], _ = precision_recall_curve(y_test.ravel(),
            y_score.ravel())
        average_precision["micro"] = average_precision_score(y_test, y_score,
                                                             average="micro")
    except:
        precision["micro"], recall["micro"], _ = precision_recall_curve(y_test.ravel(),
            model_best.predict(X_test))
        average_precision["micro"] = average_precision_score(y_test.ravel(), model_best.predict(X_test),
                                                             average="micro")

    print('Average precision score, micro-averaged over all classes: {0:0.2f}'
          .format(average_precision["micro"]))
    #########################################################

    plt.figure()
    plt.step(recall['micro'], precision['micro'], where='post')

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title(
        'Average precision score, micro-averaged over all classes: AP={0:0.2f}'
        .format(average_precision["micro"]))
    plt.grid()
    plt.title(r""+title+"")
    if save_name1 is not None:
        plt.savefig(psave_drop2 + save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()
    # setup plot details
    colors = cycle(['navy', 'turquoise', 'darkorange', 'cornflowerblue', 'teal'])

    plt.figure(figsize=(7, 8))
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    labels = []

    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)
    labels.append('iso-f1 curves')
#     l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
#     lines.append(l)
#     labels.append('micro-average Precision-recall (area = {0:0.2f})'
#                   ''.format(average_precision["micro"]))

    for i, color in zip(range(n_classes), colors):
        l, = plt.plot(recall[i], precision[i], color=color, lw=2)
        lines.append(l)
        labels.append('class {0} (area = {1:0.2f})'
                      ''.format(i, average_precision[i]))

    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.25)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
#     plt.title('Extension of Precision-Recall curve to multi-class')
    plt.title(r""+title+"")
    plt.legend(lines, labels, loc=(0.13, -.43), prop=dict(size=12))
    plt.grid()
    if save_name2 is not None:
        plt.savefig(psave_drop2 + save_name2,\
                    dpi=300,bbox_inches='tight')
    return plt

def two_complex_ROC(X,y,X_train,y_train,X_test,y_test,model_best,save_name1=None,save_name2=None,title='title'):
    from sklearn.metrics import auc
    from itertools import cycle
    "y with binarized labels"
    model_best.fit(X_train, y_train)
    try:
        y_score = model_best.predict_proba(X_test)
        y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1]]).T
#         y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1]]).T
#         y_score_n = np.asarray([y_score[0][:,1],y_score[1][:,1],y_score[2][:,1],y_score[3][:,1]]).T
    except:
        y_score = model_best.predict_proba(X_test)
        y_score_n = np.asarray([y_score[:,0],y_score[:,1]]).T
#         y_score_n = np.asarray([y_score[:,0],y_score[:,1],y_score[:,2]]).T
#     y_score_n = np.asarray([y_score[:,0],y_score[:,1]]).T
    n_classes = y.shape[1]
    n_samples, n_features = X.shape
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
#     for i in range(0,n_classes):
        try:
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score[i][:,1])
        except:
            try:
                fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score[:,i])
            except:
                fpr[i], tpr[i], _ = roc_curve(y_test,y_score[:,i])
#         fpr[i], tpr[i], _ = roc_curve(y_test[:, i],y_score_n[:,i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score_n.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    plt.figure()
    lw = 2
    plt.plot(fpr[2], tpr[2], color='darkorange',
    lw=lw, label='ROC (area = %0.2f)' % roc_auc[2])
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r""+title+"")
    plt.legend(loc="lower right")
    plt.grid()
    if save_name1 is not None:
        plt.savefig(psave_drop2 + save_name1,\
                    dpi=300,bbox_inches='tight')
    plt.show()

    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])
    # Finally average it and compute AUC
    mean_tpr /= n_classes
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])


    # Plot all ROC curves
    plt.figure()
#     plt.plot(fpr["micro"], tpr["micro"],
#         label='micro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["micro"]),
#         color='deeppink', linestyle=':', linewidth=4)
#     plt.plot(fpr["macro"], tpr["macro"],
#         label='macro-average ROC curve (area = {0:0.2f})'
#         ''.format(roc_auc["macro"]),
#         color='navy', linestyle=':', linewidth=4)
    colors = cycle(['aqua', 'darkorange', 'cornflowerblue','darkmagenta'])
    for i, color in zip(range(n_classesses), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
            label='class {0} (area = {1:0.2f})'
            ''.format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(r''+title+'')
    plt.legend(loc="lower right")
    plt.grid()
    if save_name2 is not None:
        plt.savefig(psave_drop2 + save_name2,\
                    dpi=300,bbox_inches='tight')
#     plt.savefig(psave_drop2 + "decision_trees/Kfold_DTree_ROC_E_S_S0_2.pdf",\
#                 dpi=300,bbox_inches='tight')

#     return
#     plt.show()

def plot_feature_importances(X,Y,feature_names,model):
    
    n_features = X.shape[1]
    fig = plt.figure(figsize=(10,int(n_features/2)))
    new_feature_names = []
    for i in range(len(feature_names)):
        new_feature_names.append(feature_names[i].replace('_','-'))
    plt.barh(range(n_features), model.feature_importances_, align='center',color='darkmagenta')
    plt.yticks(np.arange(n_features), new_feature_names)
    plt.xlabel("Feature importance")
    plt.ylabel("Feature")
    plt.grid()
    plt.title("Simple Decision Tree (no tunning)")
    return(plt)

import numpy as np


def _plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    """
    https://stackoverflow.com/questions/19233771/sklearn-plot-confusion-matrix-with-labels/48018785
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    accuracy = np.trace(cm) / np.sum(cm).astype('float')
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    return(plt)
    # plt.show()